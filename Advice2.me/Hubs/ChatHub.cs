﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Advice2.me.Models;
using Microsoft.AspNet.SignalR;

namespace Advice2.me.Hubs
{
    public class ChatHub : Hub
    {
        // Verilen görüşme ve kullanıcı idsi ile ilgili mesajları istek yapan client'a yollayan fonksiyon 
        public void GetMessages(int conversationId, int userId)
        {
            List<List<string>> msgs = new List<List<string>>();
            using (DBModel db = new DBModel())
            {
                conversations Con = db.conversations.Where(x => x.conversationId == conversationId).FirstOrDefault();
                if(Con.conversationAdviseeId == userId || Con.conversationConsultantId == userId)
                {
                    var Chat = db.chats.Where(x => x.chatConversationId == Con.conversationId).ToList();
                    if(Chat != null)
                    {
                        foreach(chats C in Chat)
                        {
                            Encoding utf8 = Encoding.UTF8;
                            List<string> msg = new List<string>();
                            msg.Add(C.chatSenderId.ToString());
                            msg.Add(utf8.GetString(C.chatContent));
                            msg.Add(C.chatDate.ToString());
                            msgs.Add(msg);
                        }
                        Clients.Caller.GetMessages(msgs, Chat.Count);
                    }
                }
            }
        }

        // Verilen görüşme ve kullanıcı idsi ve mesaj ile database e mesajı kayıt eden ve tüm clientlere yeni bir mesaj olduğuna dair haber veren fonskiyon.
        public void sendMessage(int conversationId, int userId, string message)
        {
            if (message != "")
            {
                using (DBModel db = new DBModel())
                {
                    conversations Con = db.conversations.Where(x => x.conversationId == conversationId).FirstOrDefault();
                    if (Con != null && (Con.conversationAdviseeId == userId || Con.conversationConsultantId == userId))
                    {
                        Encoding utf8 = Encoding.UTF8;
                        chats Chat = new chats();
                        Chat.chatConversationId = Con.conversationId;
                        Chat.chatDate = DateTime.Now;
                        Chat.chatSenderId = userId;
                        Chat.chatContent = utf8.GetBytes(message);
                        if (Con.conversationAdviseeId == userId)
                            Chat.chatRecieverId = Con.conversationConsultantId;
                        else
                            Chat.chatRecieverId = Con.conversationAdviseeId;
                        db.chats.Add(Chat);
                        db.SaveChanges();
                        Clients.All.newMessage(conversationId);
                    }
                }
            }
        }

        // Verilen görüşme ve kullanıcı idsine ait görüşmeye ait son mesajı isteği yapan client'a yollayan fonksiyon
        public void getMessage(int conversationId, int userId)
        {
            List<string> msg = new List<string>();
            using (DBModel db = new DBModel())
            {
                conversations Con = db.conversations.Where(x => x.conversationId == conversationId).FirstOrDefault();
                if (Con.conversationAdviseeId == userId || Con.conversationConsultantId == userId)
                {
                    var Chat = db.chats.Where(x => x.chatConversationId == Con.conversationId).ToList();
                    if (Chat != null)
                    {
                        Encoding utf8 = Encoding.UTF8;
                        msg.Add(Chat.Last().chatSenderId.ToString());
                        msg.Add(utf8.GetString(Chat.Last().chatContent));
                        msg.Add(Chat.Last().chatDate.ToString());
                        Clients.Caller.GetMessage(msg);
                    }
                }
            }
        }

        // Verilen görüşme ve kullanıcı idsi ve kullanıcının danışmana verdiği puanı alıp kayıt eden fonskiyon.
        public void rateConversation(int conversationId, int userId, int rate)
        {
            using (DBModel db = new DBModel())
            {
                conversations Con = db.conversations.Where(x => x.conversationId == conversationId).FirstOrDefault();
                if (Con != null && Con.conversationAdviseeId == userId)
                {
                    Con.conversationOver = true;
                    ratings Rate = new ratings();
                    Rate.ratingAdviseeId = Con.conversationAdviseeId;
                    Rate.ratingConsultantId = Con.conversationConsultantId;
                    Rate.ratingAmount = rate;
                    db.ratings.Add(Rate);
                    db.SaveChanges();
                }
            }
        }
    }
}