﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Advice2.me.Models
{
    public class Profile
    {
        public int userId { get; set; }
        public int userType { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
        public string TC { get; set; }
        public string bornDate { get; set; }
        public Boolean sex { get; set; }
        public string password { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string address { get; set; }
        public string diplomaNo { get; set; }
        public string school { get; set; }
        public string department { get; set; }
        public int expertnessType { get; set; }
        public string IBAN { get; set; }
        public bool isActive { get; set; }
        public int price { get; set; }


        // Verilen kullanıcı id'si eşliğinde database'den ilgili kullanıcıya ait bilgileri çekerek yeni bir profil nesnesi oluşturulmasını sağlayan fonksiyon
        public Profile(int userId)
        {
            this.userId = userId;
            using (DBModel db = new DBModel())
            {
                users user = db.users.Where(x => x.userId == userId).FirstOrDefault();
                if(user != null)
                {
                    userType = user.userUserType;
                    email = user.userUsername;
                    isActive = user.isActive;
                    if (user.userUserType == 1)
                    {
                        advisees Advisee = db.advisees.Where(x => x.userId == user.userId).FirstOrDefault();
                        if(Advisee != null)
                        {
                            name = Advisee.adviseeName;
                            surname = Advisee.adviseeSurname;
                            TC = Advisee.adviseeTC;
                            if(Advisee.adviseeBornDate != null)
                                bornDate = ((DateTime)Advisee.adviseeBornDate).Date.ToString();
                            if (Advisee.adviseeSex != null)
                                sex = (bool)Advisee.adviseeSex;
                            else
                                sex = true;
                            password = user.userPassword;
                            email = user.userUsername;
                            phone = Advisee.adviseePhone;
                            address = Advisee.adviseeAdress;
                        }
                    }
                    else if(user.userUserType == 2)
                    {
                        consultants Consultant = db.consultants.Where(x => x.userId == userId).FirstOrDefault();
                        if(Consultant != null)
                        {
                            name = Consultant.consultantName;
                            surname = Consultant.consultantSurname;
                            TC = Consultant.consultantTC;
                            if (Consultant.consultantBornDate != null)
                                bornDate = ((DateTime)Consultant.consultantBornDate).ToShortDateString(); //Consultant.consultantBornDate.ToString();
                            if (Consultant.consultantSex != null)
                                sex = (bool)Consultant.consultantSex;
                            else
                                sex = true;
                            password = user.userPassword;
                            phone = Consultant.consultantPhone;
                            address = Consultant.consultantAdress;
                            diplomaNo = Consultant.consultantDiplomaNo;
                            school = Consultant.consultantSchool;
                            department = Consultant.consultantDepartment;
                            if(Consultant.consultantExpertnessTypeId != null)
                                expertnessType = (int)Consultant.consultantExpertnessTypeId;
                            IBAN = Consultant.consultantIBAN;
                            if(Consultant.consultantPrice != null)
                                price = (int) Consultant.consultantPrice;
                        }
                    }
                }
            }
        }

        public Profile() { }

        // Verilen form elementeleri ve kullanıcı id'si ile kontrollü bir şekilde yeni bir profil nesnesi oluşturan fonksiyon
        public Boolean newProfile(FormCollection elems, int userId)
        {
            if (elems["password"] != elems["rePassword"])
                return false;
            this.userId = userId;
            name = elems["name"];
            surname = elems["surname"];
            if(elems["TC"].Length == 11 && isNumeric(elems["TC"]))
                TC = elems["TC"];
            try
            {
                bornDate = elems["bornDate"];
            }
            catch { }
            if (elems["sex"] == "1")
                sex = true;
            else
                sex = false;
            password = elems["password"];
            if (elems["email"] != "")
                email = elems["email"];
            phone = elems["phone"];
            address = elems["address"];
            diplomaNo = elems["diplomaNo"];
            school = elems["school"];
            department = elems["department"];
            if(elems["expertnessTypeId"] != null && isNumeric(elems["expertnessTypeId"]))
                expertnessType = Convert.ToInt32(elems["expertnessTypeId"]);
            IBAN = elems["IBAN"];
            if (elems["price"] != null && isNumeric(elems["price"]))
                price = Convert.ToInt32(elems["price"]);
            save();
            return true;
        }

        // Değişen profil bilgilerinin database'e kontrolleri yapılarak kaydedilmesini sağlayan fonksiyon
        private void save()
        {
            using (DBModel db = new DBModel())
            {
                users user = db.users.Where(x => x.userId == userId).FirstOrDefault();
                if(user != null)
                {
                    user.userUsername = email;
                    if(user.userUserType == 1)
                    {
                        advisees Advisee = db.advisees.Where(x => x.userId == user.userId).FirstOrDefault();
                        if(Advisee != null)
                        {
                            Advisee.adviseeName = name;
                            Advisee.adviseeSurname = surname;
                            Advisee.adviseeTC = TC;
                            try
                            {
                                Advisee.adviseeBornDate = Convert.ToDateTime(bornDate);
                            }
                            catch { }
                            Advisee.adviseeSex = sex;
                            user.userPassword = password;
                            Advisee.adviseePhone = phone;
                            user.userUsername = email;
                            Advisee.adviseeEmail = email;
                            Advisee.adviseeAdress = address;
                        }
                    }
                    else if(user.userUserType == 2)
                    {
                        consultants Consultant = db.consultants.Where(x => x.userId == user.userId).FirstOrDefault();
                        if(Consultant != null)
                        {
                            Consultant.consultantName = name;
                            Consultant.consultantSurname = surname;
                            Consultant.consultantTC = TC;
                            try
                            {
                                Consultant.consultantBornDate = Convert.ToDateTime(bornDate);
                            }
                            catch { }
                            Consultant.consultantSex = sex;
                            user.userPassword = password;
                            Consultant.consultantPhone = phone;
                            user.userUsername = email;
                            Consultant.consultantMail = email;
                            Consultant.consultantAdress = address;
                            Consultant.consultantDiplomaNo = diplomaNo;
                            Consultant.consultantSchool = school;
                            Consultant.consultantDepartment = department;
                            Consultant.consultantExpertnessTypeId = expertnessType;
                            Consultant.consultantIBAN = IBAN;
                            Consultant.consultantPrice = price;
                        }
                    }
                    db.SaveChanges();
                }
            }
        }

        // Verilen stringin numeric karakterlerden oluşup oluşmadığını kontrol eden fonksiyon
        public bool isNumeric(string value)
        {
            double oReturn = 0;
            return double.TryParse(value, out oReturn);
        }
    }
}