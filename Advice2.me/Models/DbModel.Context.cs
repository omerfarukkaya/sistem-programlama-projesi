﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Advice2.me.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class DBModel : DbContext
    {
        public DBModel()
            : base("name=DBModel")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<advisees> advisees { get; set; }
        public virtual DbSet<chats> chats { get; set; }
        public virtual DbSet<concact_us> concact_us { get; set; }
        public virtual DbSet<consultants> consultants { get; set; }
        public virtual DbSet<conversation_types> conversation_types { get; set; }
        public virtual DbSet<conversations> conversations { get; set; }
        public virtual DbSet<expertness_types> expertness_types { get; set; }
        public virtual DbSet<pay_method_types> pay_method_types { get; set; }
        public virtual DbSet<pay_types> pay_types { get; set; }
        public virtual DbSet<ratings> ratings { get; set; }
        public virtual DbSet<social_media_types> social_media_types { get; set; }
        public virtual DbSet<social_medias> social_medias { get; set; }
        public virtual DbSet<sysdiagrams> sysdiagrams { get; set; }
        public virtual DbSet<user_types> user_types { get; set; }
        public virtual DbSet<users> users { get; set; }
    }
}
