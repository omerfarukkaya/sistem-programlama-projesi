//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Advice2.me.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class social_medias
    {
        public int socialMediaId { get; set; }
        public int socialMediaConsultantId { get; set; }
        public string socialMediaLink { get; set; }
        public int socialMediaTypeId { get; set; }
    }
}
