﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Advice2.me.Models
{
    public class Messages
    {
        public string nameSurname { get; set; }
        public string expertness { get; set; }
        public string lastMessage { get; set; }
        public string lastMessageTime { get; set; }
        public int consultantId { get; set; }
        public string chatId { get; set; }
    }
}