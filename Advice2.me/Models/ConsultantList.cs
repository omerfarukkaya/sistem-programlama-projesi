﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Advice2.me.Models
{
    public class ConsultantList
    {
        public int consultantId { get; set; }
        public string consultantName { get; set; }
        public int consultantPrice { get; set; }
        public float consultantRate { get; set; }
        public string consultantExpertness { get; set; }
    }
}