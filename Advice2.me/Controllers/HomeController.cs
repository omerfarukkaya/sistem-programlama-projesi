﻿using Advice2.me.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Advice2.me.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        // Verilen kullanıcı bilgilerini, database'teki kullanıcı bilgileriyle karşılaştırarak eğer kullanıcı var ise ve şifresi doğruysa ilgili olduğu kullanıcı paneline yönlendiren fonksiyon
        public ActionResult Login(string email, string password)
        {
            if(email == "" || password == "")
            {
                TempData["Error"] = "E-Mail veya şifre boş bırakılamaz.";
                return Redirect(Request.UrlReferrer.ToString());
            }
            else
            {
                using (DBModel db = new DBModel())
                {
                    users user = db.users.Where(x => x.userUsername == email).Where(y => y.userPassword == password).FirstOrDefault();
                    if(user == null)
                    {
                        TempData["Error"] = "E-Mail veya şifre yanlış.";
                        return Redirect(Request.UrlReferrer.ToString());
                    }
                    else if(!user.isActive)
                    {
                        TempData["Error"] = "Üyeliğiniz askıya alınmış durumda.";
                        return Redirect(Request.UrlReferrer.ToString());
                    }
                    else if(user.userUserType == 666)
                    {
                        Session["userId"] = (user.userId).ToString();
                        return Redirect("/admin");
                    }
                    else
                    {
                        Session["userId"] = (user.userId).ToString();
                        return Redirect("/panel");
                    }
                }
            }
        }

        // Verilen kullanıcı bilgilerini alıp, yeni bir kullanıcı kaydı oluşturduktan sonra kullanıcı tipine uygun panele yönlendiren fonksiyon
        public ActionResult Register(string email, string password, string passwordConfirm, string terms, string type)
        {
            if (email == "" || password == "" || passwordConfirm == "" || terms == "" || type == "0")
            {
                TempData["ErrorReg"] = "Alanlar boş bırakılamaz.";
                return Redirect(Request.UrlReferrer.ToString());
            }
            else
            {
                using (DBModel db = new DBModel())
                {
                    users user = db.users.Where(x => x.userUsername == email).FirstOrDefault();
                    if(user == null)
                    {
                        if(password == passwordConfirm)
                        {
                            users newUser = new users();
                            newUser.userUsername = email;
                            newUser.userPassword = password;
                            newUser.userUserType = Convert.ToInt32(type);
                            newUser.isActive = true;
                            db.users.Add(newUser);
                            db.SaveChanges();
                            if(type == "1")
                            {
                                advisees Advisee = new advisees();
                                Advisee.adviseeEmail = email;
                                Advisee.userId = newUser.userId;
                                db.advisees.Add(Advisee);
                            }
                            else if(type == "2")
                            {
                                consultants Consultant = new consultants();
                                Consultant.consultantMail = email;
                                Consultant.userId = newUser.userId;
                                db.consultants.Add(Consultant);
                            }
                            db.SaveChanges();
                            Session["userId"] = (newUser.userId).ToString();
                            return Redirect("/panel/myprofile");
                        }
                        else
                        {
                            TempData["ErrorReg"] = "Şifreler birbiriyle uyuşmuyor.";
                            return Redirect(Request.UrlReferrer.ToString());
                        }
                    }
                    else
                    {
                        TempData["ErrorReg"] = "Bu e-posta adresi sistemde kayıtlı.";
                        return Redirect(Request.UrlReferrer.ToString());
                    }
                }
            }
        }

        // Parametre olarak verilen emaile kullanıcının şifresini postalayan fonksiyon
        public ActionResult sendPassword(string email)
        {
            if (email != "")
            {
                using (DBModel db = new DBModel())
                {
                    users User = db.users.Where(x => x.userUsername == email).FirstOrDefault();
                    if (User != null)
                    {
                        GMailler mail = new GMailler();
                        mail.ToEmail = User.userUsername;
                        mail.IsHtml = false;
                        mail.Subject = "Şifreniz";
                        mail.Body = "Şifreniz : " + User.userPassword;
                        mail.Send();
                        TempData["Error"] = "Şifreniz mail adresinize yollanmıştır.";
                    }
                    else
                        TempData["Error"] = "Böyle bir mail sistemde bulunamadı.";
                }
            }
            else
                TempData["Error"] = "Mail adresi boş olmamalıdır.";
            return Redirect(Request.UrlReferrer.ToString());
        }

    }
}