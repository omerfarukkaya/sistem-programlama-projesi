﻿using Advice2.me.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Advice2.me.Controllers
{
    public class PanelController : Controller
    {
        // GET: Panel

        // Sistemdeki aktif danışmanların bilgilerini alıp kullanıcıya sunulmasını sağlayan view'a bilgileri yollayıp, yönlendiren fonksiyon
        public ActionResult Index()
        {
            if (Session["userId"] == null)
                return Redirect("/");
            else
            {
                using (DBModel db = new DBModel())
                {
                    int userId = Convert.ToInt32(Session["userId"]);
                    users User = db.users.Where(x => x.userId == userId).FirstOrDefault();
                    if (User == null)
                        return Redirect("/");
                    else
                    {
                        if (User.userUserType == 1)
                        {
                            List<ConsultantList> constultants = new List<ConsultantList>();
                            var cons = db.consultants.ToList();
                            ViewBag.Bugdet = false;
                            foreach (consultants a in cons)
                            {
                                if (a.consultantPrice != null)
                                {
                                    ConsultantList c = new ConsultantList();
                                    c.consultantId = a.userId;
                                    c.consultantName = a.consultantName + " " + a.consultantSurname;
                                    c.consultantPrice = (int)a.consultantPrice;
                                    var r = db.ratings.Where(x => x.ratingConsultantId == c.consultantId).ToList();
                                    if (r != null)
                                    {
                                        foreach (var t in r)
                                            c.consultantRate += t.ratingAmount;
                                        c.consultantRate /= r.Count;
                                    }
                                    if (a.consultantExpertnessTypeId != null)
                                    {
                                        expertness_types exp = db.expertness_types.Where(x => x.expertnessTypeId == a.consultantExpertnessTypeId).FirstOrDefault();
                                        if(exp != null)
                                            c.consultantExpertness = exp.expertnessTypeName;
                                    }
                                    constultants.Add(c);
                                }
                            }
                            return View(constultants);
                        }
                        else
                            return RedirectToAction("Budget");
                    }
                }
            }
        }

        // Verilen görüşme danışman id'si ve görüşme tipi ile yeni bir görüşme oluşturan fonksiyon
        public ActionResult startNewChat(int consultantId, int type)
        {
            if (Session["userId"] == null)
                return Redirect("/");
            using (DBModel db = new DBModel())
            {
                conversations Con = new conversations();
                Con.conversationAdviseeId = Convert.ToInt32(Session["userId"]);
                Con.conversationConsultantId = consultantId;
                Con.conversationFinishTime = DateTime.Now;
                Con.conversationStartTime = DateTime.Now;
                Con.conversationPayTypeId = 1;
                Con.conversationTypeId = type;
                Con.conversationOver = false;
                db.conversations.Add(Con);
                db.SaveChanges();
                return RedirectToAction("Chat", new { conserId = Con.conversationId.ToString() });
            }
        }

        // Danışmanların bütçelerinin hesaplamalarını yapıp ilgili view'a yollayıp, yönlendiren fonksiyon
        public ActionResult Budget()
        {
            if (Session["userId"] == null)
                return Redirect("/");
            List<Budget> budget = new List<Budget>();
            using (DBModel db = new DBModel())
            {
                int total = 0;
                int userId = Convert.ToInt32(Session["userId"]);
                consultants c = db.consultants.Where(x => x.userId == userId).FirstOrDefault();
                if (c == null)
                    return RedirectToAction("Index");
                var convers = db.conversations.Where(x => x.conversationConsultantId == userId).ToList();
                foreach(var a in convers)
                {
                    if (a.conversationOver)
                    {
                        Budget bud = new Budget();
                        advisees Ads = db.advisees.Where(x => x.userId == a.conversationAdviseeId).FirstOrDefault();
                        bud.name = Ads.adviseeName + " " + Ads.adviseeSurname;
                        if (Ads.adviseeSex == null || (bool)Ads.adviseeSex)
                            bud.name += " Bey";
                        else
                            bud.name += " Hanım";

                        var Chats = db.chats.Where(x => x.chatConversationId == a.conversationId).ToList();
                        if (c.consultantPrice != null)
                        {
                            if (Chats.Count >= 2)
                            {
                                TimeSpan span = Chats.Last().chatDate.Subtract(Chats.First().chatDate);
                                if(span.Hours > 0 || span.Days > 0)
                                    bud.amount = (int)c.consultantPrice * (span.Hours + span.Days * 24);
                                else
                                    bud.amount = (int)c.consultantPrice;
                            }
                            else
                            {
                                bud.amount = (int)c.consultantPrice;
                            }
                            total += bud.amount;
                            budget.Add(bud);
                        }
                    }
                }
                if (c.consultantPrice != null)
                {
                    ViewBag.Total = total;
                }

            }
            return View(budget);
        }

        // Kullanıcının profil bilgilerini profil view'ına yollayıp, yönlendiren fonksiyon
        public ActionResult MyProfile()
        {
            if (Session["userId"] == null)
                return Redirect("/");
            using (DBModel db = new DBModel())
            {
                int userId = Convert.ToInt32(Session["userId"]);
                ViewBag.ExpertnessTypes = db.expertness_types.ToList();
                users User = db.users.Where(x => x.userId == userId).FirstOrDefault();
                if(User.userUserType == 1)
                    ViewBag.Bugdet = false;
                return View(new Profile(userId));
            }
        }
        
        // Verilen form öğeleri doğrultusunda giriş yapmış kullanıcının profil bilgilerini güncelleyen fonksiyon
        public ActionResult ProfileUpdater(FormCollection col)
        {
            if (Session["userId"] == null)
                return Redirect("/");
            int userId = Convert.ToInt32(Session["userId"]);
            Profile profile = new Profile();
            if (profile.newProfile(col, userId))
                TempData["Error"] = "Profiliniz başarıyla güncellendi.";
            else
                TempData["Error"] = "Şifreler birbiriyle eşleşmiyor.";
            return Redirect(Request.UrlReferrer.ToString());
        }

        // Kullanıcıyı contact us view'ına yönlendiren fonksiyon
        public ActionResult ContactUs()
        {
            if (Session["userId"] == null)
                return Redirect("/");
            using (DBModel db = new DBModel())
            {
                int userId = Convert.ToInt32(Session["userId"]);
                users User = db.users.Where(x => x.userId == userId).FirstOrDefault();
                if (User.userUserType == 1)
                    ViewBag.Bugdet = false;
            }
            return View();
        }

        // Kullanıcıyı hakkımızda view'ına yönlendiren fonksiyon
        public ActionResult AboutUs()
        {
            if (Session["userId"] == null)
                return Redirect("/");
            using (DBModel db = new DBModel())
            {
                int userId = Convert.ToInt32(Session["userId"]);
                users User = db.users.Where(x => x.userId == userId).FirstOrDefault();
                if (User.userUserType == 1)
                    ViewBag.Bugdet = false;
            }
            return View();
        }

        // Kullanıcın yaptığı aktif mesajlaşmaları database'den çekip ilgili view'a yollayıp, yönlendiren fonksiyon
        public ActionResult Messages()
        {
            if (Session["userId"] == null)
                return Redirect("/");
            using (DBModel db = new DBModel())
            {
                int userId = Convert.ToInt32(Session["userId"]);
                users User = db.users.Where(x => x.userId == userId).FirstOrDefault();
                if (User.userUserType == 1)
                    ViewBag.Bugdet = false;
            }
            List<Messages> msgs = new List<Messages>();
            using (DBModel db = new DBModel())
            {
                int userID = Convert.ToInt32(Session["userId"]);
                users User = db.users.Where(x => x.userId == userID).FirstOrDefault();
                List<conversations> Conservations = null;
                if (User.userUserType == 1)
                {
                    Conservations = db.conversations.Where(x => x.conversationAdviseeId == userID).ToList();
                }
                else if(User.userUserType == 2)
                {
                    Conservations = db.conversations.Where(x => x.conversationConsultantId == userID).ToList();
                }
                if (Conservations != null)
                {
                    foreach (var conser in Conservations)
                    {
                        if (!conser.conversationOver)
                        {
                            var Chats = db.chats.Where(x => x.chatConversationId == conser.conversationId).ToList();
                            Encoding utf8 = Encoding.UTF8;
                            Messages msg = new Messages();
                            if (Chats.Count != 0)
                            {
                                chats last = Chats.Last();
                                msg.lastMessage = utf8.GetString(last.chatContent);
                                msg.lastMessageTime = last.chatDate.Hour.ToString() + ":" + last.chatDate.Minute.ToString();
                            }
                            if (User.userUserType == 1)
                            {
                                consultants cons = db.consultants.Where(x => x.userId == conser.conversationConsultantId).FirstOrDefault();
                                if (cons.consultantExpertnessTypeId != null)
                                {
                                    expertness_types exp = db.expertness_types.Where(x => x.expertnessTypeId == cons.consultantExpertnessTypeId).FirstOrDefault();
                                    if (exp != null)
                                        msg.expertness = exp.expertnessTypeName;
                                    else
                                        msg.expertness = "Belirtilmemiş";
                                }
                                else
                                    msg.expertness = "Belirtilmemiş";
                                msg.consultantId = cons.userId;
                                msg.nameSurname = cons.consultantName + " " + cons.consultantSurname;

                            }
                            else if (User.userUserType == 2)
                            {
                                advisees ads = db.advisees.Where(x => x.userId == conser.conversationAdviseeId).FirstOrDefault();
                                if (conser.conversationTypeId == 1 && User.userUserType == 2)
                                {
                                    msg.nameSurname = ads.adviseeName + " " + ads.adviseeSurname;
                                    msg.consultantId = ads.userId;
                                }
                                else
                                {
                                    msg.nameSurname = "Anon-" + conser.conversationId;
                                    msg.consultantId = 0;
                                }
                                msg.expertness = "Danışan";
                            }

                            msg.chatId = conser.conversationId.ToString();
                            msgs.Add(msg);
                        }
                    }
                }
            }
            return View(msgs);
        }

        // Verilen görüşme id'sine ait chat verilerini ilgili chat view'ına yollayıp, yönlendiren fonksiyon
        public ActionResult Chat(string conserId)
        {
            if (Session["userId"] == null)
                return Redirect("/");
            if (conserId == "" || !isNumeric(conserId))
                return RedirectToAction("Index");
            using (DBModel db = new DBModel())
            {
                int userId = Convert.ToInt32(Session["userId"]);
                users User = db.users.Where(x => x.userId == userId).FirstOrDefault();
                if (User.userUserType == 1)
                    ViewBag.Bugdet = false;
            }
            int conservationId = Convert.ToInt32(conserId);
            using (DBModel db = new DBModel())
            {
                conversations Con = db.conversations.Where(x => x.conversationId == conservationId).FirstOrDefault();
                if(Con != null && !Con.conversationOver)
                {
                    int userID = Convert.ToInt32(Session["userId"]);
                    if (Con.conversationAdviseeId == userID || Con.conversationConsultantId == userID)
                    {
                        if(Con.conversationConsultantId == userID)
                        {
                            if(Con.conversationTypeId == 1)
                            {
                                advisees Advisee = db.advisees.Where(x => x.userId == Con.conversationAdviseeId).FirstOrDefault();
                                ViewBag.ChatterName = Advisee.adviseeName + " " + Advisee.adviseeSurname;
                                ViewBag.Phone = Advisee.adviseePhone;
                                ViewBag.Anon = "2";
                            }
                            else
                            {
                                ViewBag.ChatterName = "Anon-" + Con.conversationId;
                                ViewBag.Anon = "1";
                            }
                            ViewBag.Sender = Con.conversationConsultantId;
                            ViewBag.Consultant = "1";
                            ViewBag.Reciever = Con.conversationAdviseeId;
                            ViewBag.Conversation = Con.conversationId;
                            ViewBag.UserId = userID;
                        }
                        else
                        {
                            consultants Consultant = db.consultants.Where(x => x.userId == Con.conversationConsultantId).FirstOrDefault();
                            ViewBag.ChatterName = Consultant.consultantName + " " + Consultant.consultantSurname;
                            ViewBag.Sender = Con.conversationAdviseeId;
                            ViewBag.Reciever = Con.conversationConsultantId;
                            ViewBag.Conversation = Con.conversationId;
                            ViewBag.Phone = Consultant.consultantPhone;
                            ViewBag.UserId = userID;
                        }
                    }
                    else
                        return RedirectToAction("Index");
                }
                else
                    return RedirectToAction("Index");
            }
            return View();
        }

        // Tüm sessionları temizleyip kullanıcının çıkış yapmasını sağlayan fonksiyon
        public ActionResult LogOut()
        {
            Session.Clear();
            return Redirect("/");
        }

        // Verilen contact us verilerini database'e kaydeden fonksiyon
        public ActionResult ContactUsSend(string nameSurname, string email, string content)
        {
            if (Session["userId"] == null)
                return Redirect("/");
            if (nameSurname != "" && email != "" && content != "")
            {
                using (DBModel db = new DBModel())
                {
                    concact_us Contact = new concact_us();
                    Contact.contactEmail = email;
                    Contact.contactContent = content;
                    Contact.contactNameSurname = nameSurname;
                    db.concact_us.Add(Contact);
                    db.SaveChanges();
                    TempData["Error"] = "Mesajınız başarıyla iletildi.";
                }
            }
            else
                TempData["Error"] = "Alanlar boş geçilemez.";
            return Redirect(Request.UrlReferrer.ToString());
        }

        // Verilen idye ait kullanıcının fotoğrafını döndüren fonksiyon
        public ActionResult Show(int userId)
        {
            using (DBModel db = new DBModel())
            {
                byte[] imageData = null;
                users User = db.users.Where(x => x.userId == userId).FirstOrDefault();
                if(User != null)
                {
                    if(User.userUserType == 1)
                    {
                        advisees Advisee = db.advisees.Where(x => x.userId == User.userId).FirstOrDefault();
                        if (Advisee != null)
                            imageData = Advisee.adviseePhoto;
                    }
                    else if(User.userUserType == 2)
                    {
                        consultants Consultant = db.consultants.Where(x => x.userId == User.userId).FirstOrDefault();
                        if (Consultant != null)
                            imageData = Consultant.consultantPhoto;
                    }
                }
                if(imageData == null || Session["userId"] == null)
                {
                    Image defaultImage = Image.FromFile(Server.MapPath("/Assets/images/default_image.png"));
                    imageData = ImageToByte(defaultImage);
                }
                return File(imageData, "image/jpg");
            }
        }

        // Verilen resmi byte dizisine çevirip döndüren fonksiyon
        public static byte[] ImageToByte(Image img)
        {
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(img, typeof(byte[]));
        }


        // Requestten gelen fotoğrafı giriş yapmış kullanıcın profil fotoğrafı olarak güncelleyip, başarı durumunu döndüren fonksiyon
        [HttpPost]
        public ActionResult PictureUpdate()
        {
            for (int i = 0; i < Request.Files.Count; i++)
            {
                var file = Request.Files[i];
                var fileName = Path.GetFileName(file.FileName);
                using (MemoryStream ms = new MemoryStream())
                {
                    file.InputStream.CopyTo(ms);
                    byte[] array = ms.GetBuffer();
                    using (DBModel db = new DBModel())
                    {
                        int userId = Convert.ToInt32(Session["userId"]);
                        users User = db.users.Where(x => x.userId == userId).FirstOrDefault();
                        if(User != null)
                        {
                            if (User.userUserType == 1)
                            {
                                advisees Advisee = db.advisees.Where(x => x.userId == User.userId).FirstOrDefault();
                                Advisee.adviseePhoto = array;
                            }
                            else if (User.userUserType == 2)
                            {
                                consultants Consultant = db.consultants.Where(x => x.userId == User.userId).FirstOrDefault();
                                Consultant.consultantPhoto = array;
                            }
                            try
                            {
                                db.SaveChanges();
                                return Json(new { success = true, responseText = "Your message successfuly sent!" }, JsonRequestBehavior.AllowGet);
                            }
                            catch
                            {
                                return Json(new { success = false, responseText = "The attached file is not supported." }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                }
            }
            return Json(new { success = false, responseText = "The attached file is not supported." }, JsonRequestBehavior.AllowGet);


        }

        // Verilen stringin sayılardan oluşup oluşmadığını döndüren fonksiyon
        public bool isNumeric(string value) {
            double oReturn = 0;
            return double.TryParse(value, out oReturn);
        }
    }
}