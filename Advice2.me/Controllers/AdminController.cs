﻿using Advice2.me.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Advice2.me.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin

        // Tüm kullanıcıların tümünü alıp kullanıcıların listelenip engel durumlarının değiştirilebileceği view'a gönderip view'a yönlendiren fonksiyon
        public ActionResult Index()
        {
            if(!isAdmin())
                return Redirect("/");
            using (DBModel db = new DBModel())
            {
                List<Profile> prof = new List<Profile>();
                var Users = db.users.ToList();
                foreach(var user in Users)
                {
                    prof.Add(new Profile(user.userId));
                }
                return View(prof);
            }
        }

        // Contact Us'tan yazılan mesajları alıp, bağlı olduğu view'a yollayayıp view'ı açan fonksiyon
        public ActionResult Messages()
        {
            if (!isAdmin())
                return Redirect("/");
            using (DBModel db = new DBModel())
            {
                var contact = db.concact_us.ToList();
                return View(contact);
            }
        }

        // Verilen idli kullanıcıyı eğer blockluysa açan değilse blocklayan fonksiyon
        public ActionResult BlockUnblock(int userId)
        {
            if (!isAdmin())
                return Redirect("/");
            using (DBModel db = new DBModel())
            {
                users User = db.users.Where(x => x.userId == userId).FirstOrDefault();
                User.isActive = !(User.isActive);
                db.SaveChanges();
                return Redirect(Request.UrlReferrer.ToString());
            }
        }

        // Kullanıcının giriş yağıp yapmadığını ve admin olup olmadığını eğer admin ise true döndüren fonksiyon
        bool isAdmin()
        {
            if (Session["userId"] == null)
                return false;
            int userId = Convert.ToInt32(Session["userId"]);
            using (DBModel db = new DBModel())
            {
                users User = db.users.Where(x => x.userId == userId).FirstOrDefault();
                if (User != null)
                {
                    if (User.userUserType == 666)
                        return true;
                    else
                        return false;
                }
                else
                    return false;
            }
        }
    }
}